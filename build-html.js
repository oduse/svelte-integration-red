const fs = require('fs')
const path = require('path')
const rollup = require('rollup')
const svelte = require('rollup-plugin-svelte')
const { nodeResolve } = require('@rollup/plugin-node-resolve')
const commonjs = require('@rollup/plugin-commonjs')
const { createUpdateCode } = require('./src/build-utils/update.js')
const { createHtmlNode } = require('./src/build-utils/createHtmlNode.js')

function build (baseDir) {
  const packageJson = JSON.parse(fs.readFileSync(path.join(baseDir, 'package.json')))
  const nodes = packageJson['node-red'].nodes
  const version = packageJson.version
  const keys = Object.keys(nodes)
  for (const node of keys) {
    const nodeJs = path.join(baseDir, nodes[node])
    const nodeSvelte = nodeJs.replace('.js', '.svelte')
    const nodeName = node
    if (fs.existsSync(nodeSvelte)) {
      console.log(`[sir] Found svelte-file ${nodeSvelte} for node ${node}.`)

      // rewrite common functions to prevent name clashing ("use strict" mode needed for Safari leads to problem with e.g. "update" function for svelte store)
      let code = fs.readFileSync(nodeSvelte, 'utf8')
      code = code.replace(/<script.*context=("|')module("|')>/, `<script context="module">
        const render = sir_render
        const update = sir_update
        const revert = sir_revert
        const addCurrentNodeVersion = sir_addCurrentNodeVersion
      `)
      // rollup virtual seems to have problems with <script /> parts
      // Therefor saving temp file and removing it later.
      const tempFile = nodeSvelte.replace('.svelte', '_' + Date.now() + '.svelte')
      fs.writeFileSync(tempFile, code)

      // Build page
      rollup.rollup({
        input: tempFile,
        plugins: [
          svelte({
            extensions: ['.svelte'],
            emitCss: false
          }),
          nodeResolve({
            browser: true,
            dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/'),
            preferBuiltins: false
          }),
          commonjs()
        ],
        inlineDynamicImports: true,
        output: {
          format: 'cjs',
          exports: 'named'
        }
      }).then((result) => {
        result.generate({}).then((bundle) => {
          // Create wrapper html
          const wrapperFile = nodeJs.replace('.js', '.html')
          const filename = nodeJs.replace('.js', '')
          let documentationType = 'text/html'
          let documentation = 'No documentation yet.'
          if (fs.existsSync(filename + '.doc.html')) {
            documentation = fs.readFileSync(filename + '.doc.html')
          } else if (fs.existsSync(filename + '.doc.md')) {
            documentation = fs.readFileSync(filename + '.doc.md')
            documentationType = 'text/markdown'
          }
          let code = bundle.output[0].code
          // const componentName = code.match(/export default (?<component>\S+);/).groups.component
          // Remove the timestamp from the code name to prevent unnecessary file changes / commits
          const componentNameWithTimestamp = code.match(/export { (?<component>\S+) as default };/).groups.component
          const componentNameArray = componentNameWithTimestamp.split('_')
          const timestamp = componentNameArray.pop()
          const componentName = componentNameArray.join('_')
          code = code.replaceAll(componentNameWithTimestamp, componentName)
          code = code.replaceAll('_' + timestamp + '.svelte', '.svelte') // svelte comments
          // It wouldn't work as a module as then the node would get registered too late
          code = code.replace(/export { \S+ as default };/, '')
          let nodeUpdateCode = ''
          const updateNode = nodeJs.replace('.js', '-update.js')
          if (fs.existsSync(updateNode)) {
            const updateNodePath = path.resolve(updateNode)
            nodeUpdateCode = createUpdateCode(componentName, nodeName, updateNodePath, version)
          }
          const htmlNode = createHtmlNode(nodeUpdateCode, componentName, nodeName, version, code, documentationType, documentation)
          fs.writeFileSync(wrapperFile, htmlNode)
          console.log('[sir] Created HTML version of ' + nodeSvelte)
        })
      }).catch((error) => {
        console.log('[sir]', error)
      }).finally(() => {
        // remove temp file
        fs.unlinkSync(tempFile)
      })
    } else {
      console.log(`[sir] No svelte-file found for node ${node}.`)
    }
  }
}

module.exports = build
