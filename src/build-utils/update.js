function createUpdateCode (componentName, nodeName, updateFilepath, version) {
  let nodeUpdates = ''
  let { clientUpdate } = require(updateFilepath)
  if (clientUpdate) {
    if (clientUpdate.toString().trim().startsWith('function')) {
      clientUpdate = clientUpdate.replace('function', `function ${componentName}Update`)
    } else {
      clientUpdate = `const ${componentName}Update = ` + clientUpdate
    }
    nodeUpdates = `
  function parseVersion (v) {
    if (!v) v = '0.0.0'
    const versionArray = v.split('.')
    if (versionArray[2]?.includes('-')) {
      const patchAndTag = versionArray.pop().split('-')
      versionArray.push(patchAndTag[0], patchAndTag[1])
    }
    return {
      major: Number(versionArray[0] || 0),
      minor: Number(versionArray[1] || 0),
      patch: Number(versionArray[2] || 0),
      tag: versionArray[3] || '',
      string: v
    }
  }
  ${clientUpdate}
  function doUpdateOnStart () {
    RED.events.off("workspace:change", doUpdateOnStart)
    let notificationShowed = false
    let isDirty = false
    setTimeout(function () {
      RED.nodes.eachNode(node => {
        if (node.type === '${nodeName}' && node._version !== '${version}') {
          if (!notificationShowed) {
            RED.notify('Updated node ' + node.type + ' - ' + node.name + ' from _version ' + node._version + ' to ${version}. Please check the node or deploy the automatic update changes.')
            console.log('[sir] Updated node ' + node.type + ' - ' + node.name + ' from _version ' + node._version + ' to ${version}. Please check the node or deploy the automatic update changes.')
            notificationShowed = true
          }
          node = ${componentName}Update(parseVersion(node._version), node)
          node._version = '${version}'
          node.dirty = true
          node.changed = true
          isDirty = true
        }
      })
      if (isDirty) {
        RED.nodes.dirty(true)
      }
      RED.view.select("") // update UI
    }, 1000);
  }
  RED.events.on("workspace:change", doUpdateOnStart)
  `
  }
  return nodeUpdates
}

module.exports = { createUpdateCode }
